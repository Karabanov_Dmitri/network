package ru.demo.socket.inc.para;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by karab on 27.01.2018.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket=new ServerSocket(8080);
        System.out.println("сервер ждёт клиента...");

        try (Socket clientSocket=serverSocket.accept();
             InputStream inputStream=clientSocket.getInputStream();
             OutputStream outputStream=clientSocket.getOutputStream())
        {
            System.out.println("новое соединение"+clientSocket.getInetAddress().toString());
            int request;
            while ((request=inputStream.read())!=-1){
                System.out.println("прислал клиент "+request);
                outputStream.write(++request);
                System.out.println("отправил клиенту "+request);
                outputStream.flush();
            }
            System.out.println("клиент отключился");
        }
    }
}
