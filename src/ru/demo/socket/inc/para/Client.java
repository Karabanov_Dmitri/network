package ru.demo.socket.inc.para;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by karab on 27.01.2018.
 */
public class Client {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {
            int response = 1;
            outputStream.write(response);
            System.out.println("отправленно серверу " +response);

            while ((response=inputStream.read())!=-1){
                System.out.println("прислал сервер "+response);
                if (response>=10){break;}
                outputStream.write(++response);
                System.out.println("отправленно серверу "+response);
                outputStream.flush();
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
