package ru.demo.socket.inc.client;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;


/**
 * Created by karab on 27.01.2018.
 */
public class Client {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream();
             DataInputStream input = new DataInputStream(inputStream);
             DataOutputStream output = new DataOutputStream(outputStream)

        ) {
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
            String text;
            System.out.println("введите текст" );
            while (true) {
                System.out.println("ожидает ввод");
                text=bufferedReader.readLine();
                if (text.equals("конец")) {
                    System.out.println("отключение");
                    break;
                }
                System.out.println("отправленно серверу ");
                output.writeUTF(text);
                text=input.readUTF();
                System.out.println("ответ севера   "+text);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
