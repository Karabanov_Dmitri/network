package ru.demo.socket.inc.client;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by karab on 27.01.2018.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(8080);) {
            System.out.println("сервер ждёт клиента...");
            try (Socket clientSocket = serverSocket.accept();
                 InputStream inputStream = clientSocket.getInputStream();
                 OutputStream outputStream = clientSocket.getOutputStream();
                 DataInputStream input = new DataInputStream(inputStream);
                 DataOutputStream output = new DataOutputStream(outputStream);
            ) {
                while (true) {
                    output.writeUTF(input.readUTF());
                }
            }
        }catch (IOException e){
            System.out.println(e);
        }
    }
}
